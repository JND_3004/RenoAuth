package me.jdittmer.RenoAuth;

import java.sql.Timestamp;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

public class RenoAuth extends JavaPlugin implements Listener {
	
	@Override
    public void onEnable() {
		getServer().getPluginManager().registerEvents(this, this);
        System.out.println("[RenoAuth] The plugin was activated :)");
    }
	
	@Override
    public void onDisable() {
        System.out.println("[RenoAuth] The plugin was deactivated :(");
    }
	
	public static long Time(){
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		long Time = timestamp.getTime() / 1000;
		
		return Time;
	}
	
	public static String plgChatStart(){
		String title = ChatColor.GOLD + "----------------------- " + ChatColor.BOLD + "[RenoAuth]" + ChatColor.BOLD + " ----------------------";
		return title;
	}
	
	public static String plgChatEnd(){
		String end = ChatColor.GOLD + "-------------------------------------------------------------";
		return end;
	}
	
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
    	if(cmd.getName().equalsIgnoreCase("renoauth")) {
    		if(args.length == 0){
    			sender.sendMessage(plgChatStart());
    			sender.sendMessage(ChatColor.GOLD +""+ ChatColor.BOLD + "== Befehle von RenoAuth");
        		if(sender.hasPermission("renoauth.ver")){
        			sender.sendMessage(ChatColor.GOLD + "/renoauth ver");
        			sender.sendMessage(ChatColor.RESET + "Zeigt die Version des Plugins an.");
        		}
        		sender.sendMessage("");
        		if(sender.hasPermission("renoauth.register")){
        			sender.sendMessage(ChatColor.GOLD + "/renoauth register <email>");
        			sender.sendMessage(ChatColor.RESET + "Registriere dich mit deiner E-Mail Adresse in unser Forum.");
        		}
        		if(sender.hasPermission("renoauth.sync")){
        			sender.sendMessage(ChatColor.GOLD + "/renoauth sync <email>");
        			sender.sendMessage(ChatColor.RESET + "Synchronisere dein Benutzerrang zwischen unserem Server und dem Forum.");
        		}
        		sender.sendMessage(plgChatEnd());
        		return true;
    		}else{
    			if(args.length == 1){
					if(args[0].equalsIgnoreCase("ver")) {
						if(sender.hasPermission("renoauth.ver")){
	    	    			sender.sendMessage(ChatColor.GOLD +""+ ChatColor.BOLD + "== RenoAuth Version:");
	    	    			sender.sendMessage("Version: " + this.getDescription().getVersion());
	    	    			sender.sendMessage("Von Justin Dittmer (JND_3004)");
	    	    			sender.sendMessage(""+ChatColor.AQUA + ChatColor.UNDERLINE + "http://mc.jdittmer.com/");
	    	    			return true;
						}else{
							sender.sendMessage(ChatColor.GOLD + "[RenoAuth] " + ChatColor.RED + "Du hast nicht gen�gend Rechte f�r diese Aktion!");
	    	    			return true;
						}
	    			}
    			}
    			
    			if(args.length == 1){
    	    		if(args[0].equalsIgnoreCase("test")) {
    	    			sender.sendMessage("Hi " + ChatColor.BOLD + sender.getName() + ChatColor.RESET + ", this is the " + ChatColor.BOLD + "Version " + this.getDescription().getVersion() + ChatColor.RESET + " of " + ChatColor.BOLD + ChatColor.GOLD + "RenoAuth" + ChatColor.RESET + " :)");
    	    			return true;
    	    		}
    			}
    			
    			if(args[0].equalsIgnoreCase("register")){
    				if(sender.hasPermission("renoauth.register")){
    					if(args.length < 2 || args.length > 2){
    						sender.sendMessage(plgChatStart());
    						sender.sendMessage("/renoauth register <email>");
    	    				sender.sendMessage(plgChatEnd());
    	    				
    	    				return true;
    	    			}
    					
    					if(args.length == 2){
    						MySQL.InsertUsers(args[1], sender);
    						MySQL.InsertUserfields(sender);
	    	    			return true;
    	    			}
    				}else{
        				sender.sendMessage(ChatColor.GOLD + "[RenoAuth] " + ChatColor.RED + "Du hast nicht gen�gend Rechte f�r diese Aktion!");
    	    			return true;
        			}
    			}
    			
    			if(args[0].equalsIgnoreCase("sync")){
    				if(sender.hasPermission("renoauth.sync")){
    					if(args.length < 2 || args.length > 2){
    						sender.sendMessage(plgChatStart());
    						sender.sendMessage("/renoauth sync <email>");
    	    				sender.sendMessage(plgChatEnd());
    	    				
    	    				return true;
    	    			}
    					
    					if(args.length == 2){
	    	    			//MySQL.RateServer(timeDate(), timeMonth(), sender, args[1]);
    						sender.sendMessage(ChatColor.RED + "Befehl noch in der Entwicklung!");
	    	    			return true;
    	    			}
    				}else{
        				sender.sendMessage(ChatColor.GOLD + "[RenoAuth] " + ChatColor.RED + "Du hast nicht gen�gend Rechte f�r diese Aktion!");
    	    			return true;
        			}
    			}
    		}
    	}
    	
    	return false;
    }
}
