package me.jdittmer.RenoAuth;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import me.jdittmer.RenoAuth.MySQL;

public class MySQL {
	private static Connection conn = null;
	  
	private static String dbHost = "localhost";
	private static String dbPort = "3306";
	private static String database = "renocraft_forum";
	private static String dbUser = "renocraft_forum";
	private static String dbPassword = "";
 
	private MySQL() {
		try {
			Class.forName("com.mysql.jdbc.Driver");
 
			conn = DriverManager.getConnection("jdbc:mysql://" + dbHost + ":"
					+ dbPort + "/" + database + "?" + "user=" + dbUser + "&"
					+ "password=" + dbPassword);
		} catch (ClassNotFoundException e) {
			System.out.println("[RenoAuth] Driver not found!");
		} catch (SQLException e) {
			System.out.println("[RenoAuth] No connection to database!");
		}
	}
 
	private static Connection getInstance() {
		if(conn == null)
			new MySQL();
		return conn;
	}
	

	static final String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
	static SecureRandom rnd = new SecureRandom();
	
	public static String GENRandomString( int len ){
	   StringBuilder sb = new StringBuilder( len );
	   for( int i = 0; i < len; i++ ) 
	      sb.append( AB.charAt( rnd.nextInt(AB.length()) ) );
	   return sb.toString();
	}
	
	// GENERATE SALT
	public static String generate_salt(){
		return GENRandomString(8);
	}
	
	// GENERATE LOGINKEY
	public static String generate_loginkey(){
		return GENRandomString(50);
	}

	public static String md5(String password){
		String plaintext = password;
		MessageDigest m = null;
		try {
			m = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		m.reset();
		m.update(plaintext.getBytes());
		byte[] digest = m.digest();
		BigInteger bigInt = new BigInteger(1,digest);
		String hashtext = bigInt.toString(16);
		return hashtext;
	}
	
	
	// BENUTZER IN mybb_users EINTRAGEN
	public static void InsertUsers(String email, CommandSender sender)
	{
		conn = getInstance();
 
		if(conn != null) {
			Statement query;
			try {
				query = conn.createStatement();
				
				String username = sender.toString().replace("CraftPlayer{name=", "").replace("}", "");
				String password = GENRandomString(8);
				String salt = generate_salt();
				String md5_password = md5(md5(salt+password));
				String loginkey = generate_loginkey();
				String usergroup = "2";
				long regdate = RenoAuth.Time();
				long lastvisit = RenoAuth.Time();
				String threadmode = "linear";
				String showimages = "1";
				String showvideos = "1";
				String showsigs = "1";
				String showavatars = "1";
				String showquickreply = "1";
				String showredirect = "1";
				
				String checkForDoubleUser = "SELECT * FROM mybb_users WHERE email = '" + email + "'";
				ResultSet result_checkForDoubleUser = query.executeQuery(checkForDoubleUser);
				if(result_checkForDoubleUser.first() == false){
					String sql = "INSERT INTO mybb_users(username, password, salt, loginkey, email, usergroup, regdate, lastvisit, threadmode, showimages, showvideos, showsigs, showavatars, showquickreply, showredirect) " +
							"VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

					PreparedStatement preparedStatement = conn.prepareStatement(sql);
					preparedStatement.setString(1, username);
					preparedStatement.setString(2, md5_password);
					preparedStatement.setString(3, salt);
					preparedStatement.setString(4, loginkey);
					preparedStatement.setString(5, email);
					preparedStatement.setString(6, usergroup);
					preparedStatement.setLong(7, regdate);
					preparedStatement.setLong(8, lastvisit);
					preparedStatement.setString(9, threadmode);
					preparedStatement.setString(10, showimages);
					preparedStatement.setString(11, showvideos);
					preparedStatement.setString(12, showsigs);
					preparedStatement.setString(13, showavatars);
					preparedStatement.setString(14, showquickreply);
					preparedStatement.setString(15, showredirect);
					preparedStatement.executeUpdate();
				}else{
					sender.sendMessage(RenoAuth.plgChatStart());
					sender.sendMessage(ChatColor.RED + "Du hast dich bereits im Forum registriert!");
					sender.sendMessage(RenoAuth.plgChatEnd());
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	
	// BENUTZER IN mybb_userfields EINTRAGEN
	public static void InsertUserfields(CommandSender sender)
	{
		conn = getInstance();
 
		if(conn != null) {
			Statement query;
			try {
				query = conn.createStatement();
				String fid4 = sender.toString().replace("CraftPlayer{name=", "").replace("}", "");
				
				String checkForDoubleUser = "SELECT * FROM mybb_userfields WHERE fid4 = '" + fid4 +"'";
				ResultSet result_checkForDoublefid = query.executeQuery(checkForDoubleUser);
				if(result_checkForDoublefid.first() == false){
					String sql = "INSERT INTO mybb_userfields(fid4) VALUES(?)";
	
					PreparedStatement preparedStatement = conn.prepareStatement(sql);
					preparedStatement.setString(1, fid4);
					preparedStatement.executeUpdate();
					
					sender.sendMessage(RenoAuth.plgChatStart());
					sender.sendMessage(ChatColor.GREEN + "Dein Account wurde bei uns im Forum erfolgreich Registriert :)"+ ChatColor.RESET);
					sender.sendMessage("");
					sender.sendMessage("Bitte setze dein Passwort zur�ck und befolge die Anweisungen im Forum :)");
					sender.sendMessage("Passwort zur�cksetzen: " + ChatColor.GREEN + ChatColor.UNDERLINE +"https://forum.renocraft.net/member.php?action=lostpw"+ ChatColor.RESET);
					sender.sendMessage(RenoAuth.plgChatEnd());
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
}